/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rt_test1;

/**
 *
 * @author HuHuTyMoX
 */
public class Message {
    private String fullname;
    private String recipientEmail;
    private String topic;
    private String text;
    
    public Message(String fullname, String recipientEmail, String topic, String text) {
        this.fullname = fullname;
        this.recipientEmail = recipientEmail;
        this.topic = topic;
        this.text = text;
    }
    
    public String getFullname() {
        return fullname;
    }
    
    public String getRecipientEmail() {
        return recipientEmail;
    }
    
    public String getTopic() {
        return topic;
    }
    
    public String getText() {
        return text;
    }
}
