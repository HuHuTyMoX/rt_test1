/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rt_test1;

/**
 *
 * @author HuHuTyMoX
 */
public class Recipient {
    private int id;
    private String fullname;
    private String email;
    
    public Recipient(int id, String fullname, String email) {
        this.id = id;
        this.fullname = fullname;
        this.email = email;
    }
    
    public int getId() {
        return id;
    }
    
    public String getFullname() {
        return fullname;
    }
    
    public String getEmail() {
        return email;
    }
}
