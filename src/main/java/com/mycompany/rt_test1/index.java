/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rt_test1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author HuHuTyMoX
 */
public class index extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Connection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        ArrayList<Recipient> recipients = new ArrayList<>();

        String selectTableSQL = "SELECT * FROM recipients";
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                recipients.add(new Recipient(rs.getInt("id"), rs.getString("fullname"), rs.getString("email")));
            }
            dbConnection.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        request.setAttribute("recipients", recipients);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/rt_test1_base", "postgres", "pass");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return dbConnection;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        Connection dbConnection = null;
        Statement statement = null;

        String fullname = request.getParameter("last") + " " + request.getParameter("first") + " " + request.getParameter("middle");
        int idRecipient = Integer.parseInt(request.getParameter("recipient"));
        String topic = request.getParameter("topic");
        String text = request.getParameter("text");
        String insertTableSQL = "INSERT INTO messages(fullname, id_recipient, topic, text) VALUES"
                + "(\'" + fullname + "\', " + idRecipient + ", \'" + topic + "\', \'" + text + "\');";
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            statement.execute(insertTableSQL);
            dbConnection.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
