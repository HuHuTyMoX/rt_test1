﻿-- Table: messages

-- DROP TABLE messages;

CREATE TABLE messages
(
  id serial NOT NULL,
  fullname character varying(60),
  id_recipient integer,
  topic character varying(50),
  text character varying(300),
  CONSTRAINT id_message PRIMARY KEY (id),
  CONSTRAINT message_recipient FOREIGN KEY (id_recipient)
      REFERENCES recipients (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE messages
  OWNER TO postgres;
