﻿-- Database: rt_test1_base

-- DROP DATABASE rt_test1_base;

CREATE DATABASE rt_test1_base
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Russian_Russia.1251'
       LC_CTYPE = 'Russian_Russia.1251'
       CONNECTION LIMIT = -1;

