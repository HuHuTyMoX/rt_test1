﻿-- Table: recipients

-- DROP TABLE recipients;

CREATE TABLE recipients
(
  id serial NOT NULL,
  fullname character varying(50),
  email character varying(50),
  CONSTRAINT id_recipient PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE recipients
  OWNER TO postgres;
