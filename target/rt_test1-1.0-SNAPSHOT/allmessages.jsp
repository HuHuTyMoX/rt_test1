<%-- 
    Document   : allmessages
    Created on : 28.07.2016, 20:39:00
    Author     : HuHuTyMoX
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">         
        <title>Просмотр заявок</title>
    </head>
    <body>
        <h1 class="col-md-offset-2">Просмотр заявок</h1>
        <div class="table-responsive">
            <table class="table table-striped table-condensed">
                <tr>
                    <td><b>#</b></td>
                    <td><b>ФИО отправителя</b></td>
                    <td><b>E-mail получателя</b></td>
                    <td><b>Тема сообщения</b></td>
                    <td><b>Текст сообщения</b></td>
                </tr>
                <%! int i = 0;%>
                <c:forEach var="message" items="${messages}">
                    <tr>
                        <td><% out.print(++i); %></td>
                        <td>${message.fullname}</td>
                        <td>${message.recipientEmail}</td>
                        <td>${message.topic}</td>
                        <td>${message.text}</td>
                    </tr>
                </c:forEach>
                <% i = 0;%>
            </table>
        </div>
    </form>    
</body>
</html>
