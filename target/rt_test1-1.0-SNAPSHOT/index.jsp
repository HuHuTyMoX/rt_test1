<%-- 
    Document   : index
    Created on : 27.07.2016, 17:06:05
    Author     : HuHuTyMoX
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">       
        <script type="text/javascript" src="resources/css/bootstrap.min.css"></script>   

        <title>Форма обратной связи</title>
    </head>
    <body>
        <h1 class="col-md-offset-2">Форма обратной связи</h1>
        <div class="row">
            <div class="col-md-6 col-md-offset-2">
                <form role="form" action="" method="post">
                    <div class="form-group">
                        <label for="last">Фамилия</label>
                        <input type="text" class="form-control" name="last" placeholder="Введите фамилию" required maxlength="19">
                    </div>
                    <div class="form-group">                        
                        <label for="first">Имя</label>
                        <input type="text" class="form-control" name="first" placeholder="Введите имя" required maxlength="19">
                    </div>
                    <div class="form-group">
                        <label for="middle">Отчество</label>
                        <input type="text" class="form-control" name="middle" placeholder="Введите отчество" required maxlength="19">
                    </div>
                    <label for="recip">Получатель</label>
                    <select class="form-group" name="recipient" required>
                        <c:forEach var="recipient" items="${recipients}">
                            <option  value="${ recipient.id }"><c:out value="${ recipient.fullname } ${recipient.email}"/></option>
                        </c:forEach>
                    </select>
                    <div class="form-group">
                        <label for="topic">Тема</label>
                        <input type="text" class="form-control" name="topic" placeholder="Введите тему сообщения" required maxlength="50">
                    </div>
                    <div class="form-group">
                        <label for="text">Текст сообщения</label>
                        <textarea class="form-control" name="text" placeholder="Введите сообщение" rows="4" required maxlength="300"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Отправить</button>
                </form>
                <form class="form-horizontal" method="post" action="messages">
                    <button class="btn" type="submit">Список заявок</button>
                </form>
            </div>
        </div>
    </body>
</html>
